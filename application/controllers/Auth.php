<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Auth extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("user_model");
		
	}
	
	public function index()
	
	{
		//cek login akses
		$user_login =$this->session->userdata();
		
		if (count($user_login)<= 1){
			$this->login();
		}else{
			redirect("home/");
		}
	}
	public function login()
	
	{
		if (!empty($_REQUEST)) {
			//ambil dari form login
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$data_login = $this->user_model->cekuser($username, $password);
			
			//echo "<pre>";
			//print_r($data_login); die();
			//echo "</pre>";
			
			$data_sesi = array(
				'username' => $data_login['nik'],
				'email' => $data_login['email'],
				'tipe' => $data_login['tipe'],
				'status' => "login"
			);
			
			if (!empty($data_login)) {
				//login berhasil
				$this->session->set_userdata($data_sesi);
				redirect("home/", "refresh");
			}else{
				//login gagal
				$this->session->set_flashdata('info', 'Username atau Password Salah!');
				redirect("auth/", "refresh");
			}
		}
		$this->load->view('login');
	}
	
	//public function home()
	//{
	//	$this->load->view('welcome_message_table');	
	//}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect("auth/", "refresh");
	}
	
}